# Roperty Generator

This application creates random properties and stores them using Roperty.

## Building

    mvn clean package
    
## Running

Warning: The relations within the database configured in persistence.xml (see below) will be dropped.

    java -jar target/*.jar
    
## Configuration

### application.properties

Location: src/main/resources

- _numberOfKeys_ and _valuesPerKey_ The number of keys to create. For each key, valuesPerKey values will be generated.
- _keyDepth_ Each key is an aggregation of keyDepth words divided by slashes
- _descriptionMinLength_ and _descriptionMaxLength_ The number of words the description has to be

### persistence.xml

Location: src/main/resources/META-INF

Contains the JPA connection information.
