package com.parship.roperty.generator;

import com.parship.roperty.Roperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.Random;

@SpringBootApplication
public class RopertyGeneratorApp {

    private static final Logger LOGGER = LoggerFactory.getLogger(RopertyGeneratorApp.class);

    @Autowired
    private WordList wordList;

    @Autowired
    private Random random;

    @Autowired
    private Roperty roperty;

    @Autowired
    private ValuesGenerator valuesGenerator;

    @Value("${numberOfKeys}")
    private int numberOfKeys;

    @Value("${keyDepth}")
    private int keyDepth;

    public static void main(String[] args) {
        SpringApplication.run(RopertyGeneratorApp.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext context) {
        return args -> {

            for (int keyCount = 0; keyCount < numberOfKeys; keyCount++) {
                float progress = 100f * keyCount / numberOfKeys;
                if (progress % 1 == 0) {
                    LOGGER.info("Creating key {} of {} ({} %)", keyCount, numberOfKeys, progress);
                }
                String key = createKey();
                valuesGenerator.createValues(key);
            }

            LOGGER.info("Created {} keys", numberOfKeys);
            SpringApplication.exit(context);
        };
    }

    private String createKey() {
        StringBuilder key = new StringBuilder();
        int depth = random.nextInt(keyDepth) + 1;
        for (int curDepth = 0; curDepth < depth; curDepth++) {
            key.append('/');
            key.append(wordList.nextRandomWord());
        }
        return key.toString();
    }

}
