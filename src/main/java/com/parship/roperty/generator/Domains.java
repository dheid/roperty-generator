package com.parship.roperty.generator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daniel on 30.03.17.
 */
public class Domains {

    private List<String> domains = new ArrayList<>();

    public boolean add(String domain) {
        return domains.add(domain);
    }

    public String[] toArray() {
        return domains.toArray(new String[domains.size()]);
    }

    public int size() {
        return domains.size();
    }
}
