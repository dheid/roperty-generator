package com.parship.roperty.generator;

import com.parship.roperty.Roperty;
import de.svenjacobs.loremipsum.LoremIpsum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by daniel on 30.03.17.
 */
public class ValuesGenerator {

    private static final int MAX_START_INDEX = 50;

    @Autowired
    private LoremIpsum loremIpsum;

    @Autowired
    private WordList wordList;

    @Autowired
    private Random random;

    @Autowired
    private Roperty roperty;

    @Autowired
    private Domains domains;

    @Value("${valuesPerKey}")
    private int valuesPerKey;

    @Value("${descriptionMaxLength}")
    private int descriptionMaxLength;

    @Value("${descriptionMinLength}")
    private int descriptionMinLength;


    public void createValues(String key) {
        for (int valuesPerKeyCount = 0; valuesPerKeyCount < valuesPerKey; valuesPerKeyCount++) {
            createValue(key);
        }
    }

    private void createValue(String key) {
        String value = wordList.nextRandomWord();
        String description = createDescription();
        String changeSet = wordList.nextRandomWord();

        int maxDomainLevel = random.nextInt(domains.size());
        List<String> domainValues = new ArrayList<>(maxDomainLevel);
        for (int domainLevel = 0; domainLevel < maxDomainLevel; domainLevel++) {
            domainValues.add(wordList.nextRandomWord());
        }

        roperty.setWithChangeSet(
                key,
                value,
                description,
                changeSet,
                domainValues.toArray(new String[maxDomainLevel]));
    }

    private String createDescription() {
        int descriptionLength = random.nextInt(descriptionMaxLength) + descriptionMinLength;
        int startIndex = random.nextInt(MAX_START_INDEX);
        return loremIpsum.getWords(descriptionLength, startIndex);
    }
}
