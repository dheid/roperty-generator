package com.parship.roperty.generator;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by daniel on 30.03.17.
 */
public class DomainsFactory {

    @Autowired
    private WordList wordList;

    public Domains create() {
        Domains domains = new Domains();
        for (int domainDepth = 0; domainDepth < 4; domainDepth++) {
            domains.add(wordList.nextRandomWord());
        }
        return domains;
    }

}
