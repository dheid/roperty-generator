package com.parship.roperty.generator;

import com.parship.roperty.Persistence;
import com.parship.roperty.Roperty;
import com.parship.roperty.RopertyImpl;
import com.parship.roperty.persistence.jpa.LazyJpaPersistence;
import com.parship.roperty.persistence.jpa.QueryBuilder;
import com.parship.roperty.persistence.jpa.QueryBuilderDelegate;
import com.parship.roperty.persistence.jpa.RopertyKey;
import com.parship.roperty.persistence.jpa.RopertyKeyDAO;
import com.parship.roperty.persistence.jpa.RopertyValue;
import com.parship.roperty.persistence.jpa.RopertyValueDAO;
import com.parship.roperty.persistence.jpa.TransactionManager;
import de.svenjacobs.loremipsum.LoremIpsum;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManagerFactory;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Random;

@Configuration
public class RopertyGeneratorConfig {

    @Bean
    public Persistence persistence() {
        LazyJpaPersistence sourcePersistence = new LazyJpaPersistence();
        sourcePersistence.setTransactionManager(transactionManager());
        sourcePersistence.setRopertyKeyDAO(ropertyKeyDAO());
        sourcePersistence.setRopertyValueDAO(ropertyValueDAO());
        return sourcePersistence;
    }

    @Bean
    public LoremIpsum loremIpsum() {
        return new LoremIpsum();
    }

    @Bean
    public Random random() {
        return new Random();
    }

    @Bean
    public Roperty roperty() {
        return new RopertyImpl(persistence(), domains().toArray());
    }

    @Bean
    DomainsFactory domainsFactory() {
        return new DomainsFactory();
    }

    @Bean
    public ValuesGenerator valuesGenerator() {
        return new ValuesGenerator();
    }

    @Bean
    public Domains domains() {
        return domainsFactory().create();
    }

    @Bean
    RopertyValueDAO ropertyValueDAO() {
        RopertyValueDAO ropertyValueDAO = new RopertyValueDAO();
        ropertyValueDAO.setQueryBuilderDelegate(valueQueryBuilderDelegate());
        return ropertyValueDAO;
    }

    @Bean
    QueryBuilderDelegate<RopertyValue> valueQueryBuilderDelegate() {
        QueryBuilderDelegate<RopertyValue> valueQueryBuilderDelegate = new QueryBuilderDelegate<>();
        valueQueryBuilderDelegate.setEntityManagerFactory(entityManagerFactory());
        valueQueryBuilderDelegate.setQueryBuilder(new QueryBuilder<>());
        valueQueryBuilderDelegate.setResultClass(RopertyValue.class);
        return valueQueryBuilderDelegate;
    }

    @Bean
    RopertyKeyDAO ropertyKeyDAO() {
        RopertyKeyDAO ropertyKeyDAO = new RopertyKeyDAO();
        ropertyKeyDAO.setQueryBuilderDelegate(keyQueryBuilderDelegate());
        return ropertyKeyDAO;
    }

    @Bean
    QueryBuilderDelegate<RopertyKey> keyQueryBuilderDelegate() {
        QueryBuilderDelegate<RopertyKey> keyQueryBuilderDelegate = new QueryBuilderDelegate<>();
        keyQueryBuilderDelegate.setEntityManagerFactory(entityManagerFactory());
        keyQueryBuilderDelegate.setQueryBuilder(new QueryBuilder<>());
        keyQueryBuilderDelegate.setResultClass(RopertyKey.class);
        return keyQueryBuilderDelegate;
    }

    @Bean
    EntityManagerFactory entityManagerFactory() {
        return javax.persistence.Persistence.createEntityManagerFactory("postgresql");
    }

    @Bean
    TransactionManager transactionManager() {
        TransactionManager transactionManager = new TransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory());
        return transactionManager;
    }

    @Bean
    WordList wordList() throws URISyntaxException {
        URI wordListUri = ClassLoader.getSystemResource("words").toURI();
        WordListFactory factory = new WordListFactory(wordListUri);
        return factory.create();
    }

}