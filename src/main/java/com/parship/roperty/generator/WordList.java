package com.parship.roperty.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by daniel on 30.03.17.
 */
public class WordList {

    private List<String> words = new ArrayList<>();

    private Random random = new Random();

    public boolean add(String word) {
        return words.add(word);
    }

    public String nextRandomWord() {
        return words.get(random.nextInt(words.size()));
    }
}
