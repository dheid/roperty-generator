package com.parship.roperty.generator;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * Created by daniel on 30.03.17.
 */
public class WordListFactory {

    private final URI wordListUri;

    public WordListFactory(URI wordListUri) {
        this.wordListUri = wordListUri;
    }

    public WordList create() {
        WordList wordList = new WordList();

        try (Stream<String> stream = Files.lines(Paths.get(wordListUri))) {
            stream.forEach(wordList::add);
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        return wordList;
    }

}
